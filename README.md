# Secant_Senpai's Machine Learning Portfolio as Microservices

So I've been thinking to integrate whatever I have learned about machine learning (and deep learning) as loosely-coupled, individually deployable applications.

```
Current Progress:
Multiclass Image Classification (Intel's Natural Scenes Dataset from Kaggle)
```
The code might be a little rough on the edges at this point in time, but I will surely improve it over time.

----

