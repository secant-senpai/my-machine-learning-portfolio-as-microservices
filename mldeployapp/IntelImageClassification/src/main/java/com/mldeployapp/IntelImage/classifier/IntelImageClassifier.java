package com.mldeployapp.IntelImage.classifier;

import lombok.extern.slf4j.Slf4j;
import org.deeplearning4j.core.storage.StatsStorage;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.InvocationType;
import org.deeplearning4j.optimize.listeners.EvaluativeListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.model.stats.StatsListener;
import org.deeplearning4j.ui.model.storage.InMemoryStatsStorage;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.SqueezeNet;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.ScheduleType;
import org.nd4j.linalg.schedule.StepSchedule;

import java.io.File;
import java.io.IOException;

/**
 *  ,---.                                      ,--.           ,---.                                     ,--.
 * '   .-'   ,---.   ,---.  ,--,--. ,--,--,  ,-'  '-.        '   .-'   ,---.  ,--,--,   ,---.   ,--,--. `--'
 * `.  `-.  | .-. : | .--' ' ,-.  | |      \ '-.  .-'        `.  `-.  | .-. : |      \ | .-. | ' ,-.  | ,--.
 * .-'    | \   --. \ `--. \ '-'  | |  ||  |   |  |   ,----. .-'    | \   --. |  ||  | | '-' ' \ '-'  | |  |
 * `-----'   `----'  `---'  `--`--' `--''--'   `--'   '----' `-----'   `----' `--''--' |  |-'   `--`--' `--'
 *                                                                                     `--'
 * @author Jason Har
 * Repo: https://gitlab.com/secant-senpai/my-machine-learning-portfolio-as-microservices
 *
 *
 * Multiclass Image Classifier Model
 * Defines available models: SqueezeNet, Convolutional Neural Network.
 *
 *
 */
@Slf4j
public class IntelImageClassifier {

    // Hyperparameters
    private static int batchSize = 32;      // Subject to memory constraints
    private static double lr = 0.001;       // Learning rate
    private static int numEpochs = 10;      // Number of epochs to train for

    private static int seed = 123;

    // Filename to save the model as
    private static File modelFileName = new File("/generated-models/IntelImageClassificationModel.zip");


    /**
     * Train Classifier
     * @throws IOException
     *
     */
    public static void trainClassifier() throws IOException {

        IntelImageDataSetIterator.setup(batchSize);
        DataSetIterator trainingIter = IntelImageDataSetIterator.trainIterator();
        DataSetIterator testIter = IntelImageDataSetIterator.testIterator();

        ComputationGraph model = createModelSqueezenet();
//        MultiLayerNetwork model = new MultiLayerNetwork(createCNN());

        model.init();
        log.info(model.summary());

        // Initialize DL4J UI, can be accessed at http://localhost:9000
        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new InMemoryStatsStorage();
        uiServer.attach(statsStorage);

        // Setting listeners for training metrics
        model.setListeners(
                new StatsListener(statsStorage),
                new ScoreIterationListener(5),
                new EvaluativeListener(trainingIter, 1, InvocationType.EPOCH_END),
                new EvaluativeListener(testIter, 1, InvocationType.EPOCH_END)
        );

        // For each iteration, train model on the training set
        for (int iteration = 0; iteration < numEpochs; iteration++) {
            model.fit(trainingIter);
        }

        // Evaluation
        Evaluation trainEval = model.evaluate(trainingIter);
        Evaluation testEval = model.evaluate(testIter);
        log.info("================ Training Set Evaluation: =========================");
        log.info(trainEval.stats());
        log.info("================= Test Set Evaluation: ============================");
        log.info(testEval.stats());

        // Export trained model.
        model.save(modelFileName, true);
        log.info("Model build complete.");
    }

    /**
     * SqueezeNet model (Via Transfer Learning)
     * @return ComputationGraph SqueezeNet
     * @throws IOException
     *
     */
    public static ComputationGraph createModelSqueezenet() throws IOException {

        // SqueezeNet configuration
        ZooModel transferLearningModel = SqueezeNet.builder().build();
        ComputationGraph squeezenet = (ComputationGraph) transferLearningModel.initPretrained();
        log.info(squeezenet.summary());     // Print model architecture before transfer learning.

        FineTuneConfiguration conf = FineTuneConfiguration.builder()
                .seed(seed)
                .weightInit(WeightInit.XAVIER)
                // Optimizer
                .updater(new Adam(new StepSchedule(ScheduleType.EPOCH, lr, 0.5, 5)))
                // Better memory management
                .trainingWorkspaceMode(WorkspaceMode.ENABLED)
                .inferenceWorkspaceMode(WorkspaceMode.ENABLED)
                .build();

        return new TransferLearning.GraphBuilder(squeezenet)
                .fineTuneConfiguration(conf)
//                .setFeatureExtractor("fire9_concat")
                .removeVertexKeepConnections("conv10")
                .removeVertexAndConnections("relu10")
                .removeVertexAndConnections("global_average_pooling2d_5")
                .removeVertexAndConnections("loss")
                .addLayer("conv10_small", new ConvolutionLayer.Builder(1, 1)
                        .nIn(512)
                        // Number of Classes to predict
                        .nOut(IntelImageDataSetIterator.getNumClasses())
                        .build(), "drop9")
                .addLayer("conv10_activ", new ActivationLayer(Activation.RELU), "conv10_small")
                .addLayer("global_average_pooling", new GlobalPoolingLayer(PoolingType.MAX), "global_average_pooling")
                // Output Layer
                .addLayer("relu10", new ActivationLayer(Activation.SOFTMAX), "conv10_activ")
                .addLayer("loss", new LossLayer.Builder(LossFunctions.LossFunction.MCXENT).build(), "relu10")
                .setOutputs("loss")
                // Image classification
                .setInputTypes(InputType.convolutional(
                        IntelImageDataSetIterator.getHeight(),
                        IntelImageDataSetIterator.getWidth(),
                        IntelImageDataSetIterator.getChannels()
                ))
                .build();

    }

    /**
     * Convolutional Neural Network (Sequential layers built manually, similar to Keras)
     * @return MultiLayerConfiguration CNN_Model
     *
     */
    public static MultiLayerConfiguration createCNN() {

        // Sample CNN obtained from:
        // https://www.kaggle.com/code/ahmadjaved097/multiclass-image-classification-using-cnn

        return new NeuralNetConfiguration.Builder()
                .seed(seed)
                .weightInit(WeightInit.XAVIER)
                .updater(new Adam(new StepSchedule(ScheduleType.EPOCH, lr, 0.5, 5)))
                .list()
                .layer(0, new ConvolutionLayer.Builder(new int[]{5, 5})
                        .name("conv1")
                        .convolutionMode(ConvolutionMode.Truncate)
                        .activation(Activation.RELU)
                        .nIn(IntelImageDataSetIterator.getChannels())     // Color image
                        .nOut(128)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX,
                        new int[]{2, 2})
                        .name("maxpool1")
                        .build())
                .layer(2, new BatchNormalization.Builder().build())
                .layer(3, new ConvolutionLayer.Builder(new int[]{3, 3})
                        .name("conv2")
                        .convolutionMode(ConvolutionMode.Truncate)
                        .l2(0.00005)
                        .activation(Activation.RELU)
                        .nOut(64)
                        .build())
                .layer(4, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX,
                        new int[]{2, 2})
                        .name("maxpool2")
                        .build())
                .layer(5, new BatchNormalization.Builder().build())
                .layer(6, new ConvolutionLayer.Builder(new int[]{3, 3})
                        .name("conv3")
                        .convolutionMode(ConvolutionMode.Truncate)
                        .l2(0.00005)
                        .activation(Activation.RELU)
                        .nOut(32)
                        .build())
                .layer(7, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX,
                        new int[]{2, 2})
                        .name("maxpool3")
                        .build())
                .layer(8, new BatchNormalization.Builder().build())
                .layer(9, new DenseLayer.Builder()
                        .name("dense1")
                        .weightInit(WeightInit.XAVIER)
                        .nOut(256)
                        .dropOut(0.5)
                        .build())
                .layer(10, new DropoutLayer.Builder(0.5).build())
                .layer(11, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .name("Output")
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SOFTMAX)
                        .nOut(IntelImageDataSetIterator.getNumClasses())                    // Number of Classes to predict
                        .build())
                .setInputType(InputType.convolutional(150, 150, 3))     // Image classification
                .build();

    }
}
