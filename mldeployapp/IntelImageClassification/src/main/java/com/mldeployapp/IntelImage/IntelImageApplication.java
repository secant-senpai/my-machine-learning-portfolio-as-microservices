package com.mldeployapp.IntelImage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntelImageApplication {
    public static void main(String[] args) {
        SpringApplication.run(IntelImageApplication.class, args);
    }
}
