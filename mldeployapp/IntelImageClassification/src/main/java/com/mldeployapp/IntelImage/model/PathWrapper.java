package com.mldeployapp.IntelImage.model;

import java.io.File;

public class PathWrapper {

    private final String predictedFileDir;
    private final File modelFileName;

    public PathWrapper(String predictedFileDir, String modelFileName) {
        this.predictedFileDir = predictedFileDir;
        this.modelFileName = new File(modelFileName);
    }

    public String getPredictedFileDir() {
        return predictedFileDir;
    }

    public File getModelFileName() {
        return modelFileName;
    }
}
