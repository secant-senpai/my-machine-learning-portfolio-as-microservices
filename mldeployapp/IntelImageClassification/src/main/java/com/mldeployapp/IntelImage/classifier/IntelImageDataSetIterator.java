package com.mldeployapp.IntelImage.classifier;

import com.mldeployapp.IntelImage.Helper;
import lombok.extern.slf4j.Slf4j;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.BaseImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.*;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *  ,---.                                      ,--.           ,---.                                     ,--.
 * '   .-'   ,---.   ,---.  ,--,--. ,--,--,  ,-'  '-.        '   .-'   ,---.  ,--,--,   ,---.   ,--,--. `--'
 * `.  `-.  | .-. : | .--' ' ,-.  | |      \ '-.  .-'        `.  `-.  | .-. : |      \ | .-. | ' ,-.  | ,--.
 * .-'    | \   --. \ `--. \ '-'  | |  ||  |   |  |   ,----. .-'    | \   --. |  ||  | | '-' ' \ '-'  | |  |
 * `-----'   `----'  `---'  `--`--' `--''--'   `--'   '----' `-----'   `----' `--''--' |  |-'   `--`--' `--'
 *                                                                                     `--'
 * @author Jason Har
 * Repo: https://gitlab.com/secant-senpai/my-machine-learning-portfolio-as-microservices
 *
 *
 * Image Preprocessing for Training Data
 * Prepares local images for model training: handles image transformation and creates iterables.
 *
 *
 */
@Slf4j
public class IntelImageDataSetIterator {

    private static String dataDir;
    private static String trainDirPath = "IntelImageClassification/seg_train";
    private static String testDirPath = "IntelImageClassification/seg_test";

    // Image information
    private static final int height = 150;
    private static final int width = 150;
    private static final int channels = 3;      // Color channels
    private static int numClasses;

    // Constants
    private static final String[] allowedExtensions = BaseImageLoader.ALLOWED_FORMATS;
    private static final Random rng = new Random(123);
    private static ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();

    // Training and Test Data
    private static FileSplit trainData, testData;
    private static DataNormalization normalizer = new ImagePreProcessingScaler(0, 1);   // Data Normalization
    private static ImageTransform transformer = null;

    // Training information
    private static int batchSize;

    // Image Data Loading
    public static void setup(int batchSizeArg) throws IOException {

        // Defines the parent folder where all the files are located. Dataset is obtained from Kaggle:
        // Intel Image Classification Dataset (https://www.kaggle.com/datasets/puneet6060/intel-image-classification)
        dataDir = Paths.get(
                System.getProperty("user.home"),
                Helper.getPropValues("dl4j_home.data")
        ).toString();

        batchSize = batchSizeArg;

        // DIRECTORY STRUCTURE:
        // Images in the dataset have to be organized in directories by class/label.
        // In this example there are images in three classes
        // Here is the directory structure
        //                                    parentDir
        //                                  /    |     \
        //                                 /     |      \
        //                            labelA  labelB   labelC
        //
        // Data were set up like this so that labels from each label/class live in their own directory
        // And these label/class directories live together in the parent directory

        // TODO: Handle file not found error

        File trainDir = new File(Paths.get(dataDir, trainDirPath).toString());
        File testDir = new File(Paths.get(dataDir, testDirPath).toString());

        trainData = new FileSplit(trainDir, allowedExtensions, rng);
        testData = new FileSplit(testDir, allowedExtensions, rng);

//        BalancedPathFilter pathFilter = new BalancedPathFilter(rng, allowedExtensions, labelMaker);

        transformer = createTransformPipeline(rng);     // OPTIONAL: transform images for more variety in training

    }

    public static ImageTransform createTransformPipeline(Random rng) {

        FlipImageTransform horizontalFlip = new FlipImageTransform(1);
        ImageTransform cropImage = new CropImageTransform(5);
        ImageTransform rotateImage = new RotateImageTransform(rng, 15);
        List<Pair<ImageTransform, Double>> transformPipeline = Arrays.asList(
                new Pair<>(horizontalFlip, 0.5),
                new Pair<>(cropImage, 0.5),
                new Pair<>(rotateImage, 0.3)
        ) ;
        return new PipelineImageTransform(transformPipeline);

    }

    public static DataSetIterator trainIterator() throws IOException {
        return makeIterator(trainData, true);
    }

    public static DataSetIterator testIterator() throws IOException {
        return makeIterator(testData, false);
    }

    // Makes data iterables for use in model training and model evaluation.
    public static DataSetIterator makeIterator(FileSplit data, boolean isTrainingData) throws IOException {

        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);

        if (isTrainingData && transformer != null) {
            // Transform training set for more variety (Better flexibility in predicting)
            recordReader.initialize(data, transformer);
            // Total of 6 classes to predict: buildings, forest, glacier, mountain, sea, street
            numClasses = recordReader.numLabels();
        } else {
            // Leave the test set as original
            recordReader.initialize(data);
        }

        DataSetIterator iter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numClasses);
        iter.setPreProcessor(normalizer);

        return iter;

    }

    public static int getNumClasses() {
        return numClasses;
    }

    public static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }

    public static int getChannels() {
        return channels;
    }
}
