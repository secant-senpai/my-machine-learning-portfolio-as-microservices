package com.mldeployapp.IntelImage.classifier;

import lombok.extern.slf4j.Slf4j;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.common.io.ClassPathResource;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.io.File;
import java.io.IOException;

/**
 *  ,---.                                      ,--.           ,---.                                     ,--.
 * '   .-'   ,---.   ,---.  ,--,--. ,--,--,  ,-'  '-.        '   .-'   ,---.  ,--,--,   ,---.   ,--,--. `--'
 * `.  `-.  | .-. : | .--' ' ,-.  | |      \ '-.  .-'        `.  `-.  | .-. : |      \ | .-. | ' ,-.  | ,--.
 * .-'    | \   --. \ `--. \ '-'  | |  ||  |   |  |   ,----. .-'    | \   --. |  ||  | | '-' ' \ '-'  | |  |
 * `-----'   `----'  `---'  `--`--' `--''--'   `--'   '----' `-----'   `----' `--''--' |  |-'   `--`--' `--'
 *                                                                                     `--'
 * @author Jason Har
 * Repo: https://gitlab.com/secant-senpai/my-machine-learning-portfolio-as-microservices
 *
 *
 * Predictor For New Data
 * Groups local images by category: buildings, forest, glacier, mountain, sea, street.
 *
 *
 */
@Slf4j
public class IntelImagePredictor {

    // Path for images
    private String placeholderPredictedFileName = "/IntelImageClassification/seg_pred";
    // Saved model filename
    private static File placeholderModelFileName = new File("/generated-models/IntelImageClassificationModel.zip");

    /**
     * Predict Using Classifier (IntelImageClassifier.java)
     * @param predictedFilesDir
     * @param modelFileName
     * @throws IOException
     */
    public void predict(String predictedFilesDir, File modelFileName) throws IOException {

        // Import model
        ComputationGraph loadedModel = ComputationGraph.load(placeholderModelFileName, true);

        // Making inference on prediction set.
        NativeImageLoader imageLoader = new NativeImageLoader(
                IntelImageDataSetIterator.getHeight(),
                IntelImageDataSetIterator.getWidth(),
                IntelImageDataSetIterator.getChannels(),
                true
        );
        File[] filesToPredict = new ClassPathResource(placeholderPredictedFileName).getFile().listFiles();
        for (int count = 0; count < filesToPredict.length; count++) {
            INDArray inferenceInput = imageLoader.asMatrix(filesToPredict[count]);
            INDArray[] predictions = loadedModel.output(false, inferenceInput);
            log.info("Image # " + count + ", Prediction: " + predictions[0]);
        }

        log.info("Prediction done.");

    }

}
