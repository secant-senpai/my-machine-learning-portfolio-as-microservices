package com.mldeployapp.IntelImage.api;

import com.mldeployapp.IntelImage.model.PathWrapper;
import com.mldeployapp.IntelImage.service.IntelImageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Slf4j
@RequestMapping("/api/v1/intel")
@RestController
@AllArgsConstructor
public class IntelImageController {

    private final IntelImageService intelImageService;

    @GetMapping(name = "/get")
    public ResponseEntity<String> getPredictions(@RequestBody PathWrapper userInput) throws IOException {
        // TODO: Handle file not found error
        intelImageService.getPredictions(userInput);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Server not yet ready.");
    }

    @GetMapping(name = "/train")
    public Map<String, String> trainModel() throws IOException {
        // TODO: Handle training performance (return score as status)
        int status = intelImageService.trainModel();
        if (status < 1) {
            return Collections.singletonMap("Error", "Model could not be run.");
        } else return Collections.singletonMap("Error", "Server not yet ready.");
    }

}
