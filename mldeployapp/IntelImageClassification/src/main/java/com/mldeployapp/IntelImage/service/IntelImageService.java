package com.mldeployapp.IntelImage.service;

import com.mldeployapp.IntelImage.classifier.IntelImageClassifier;
import com.mldeployapp.IntelImage.classifier.IntelImagePredictor;
import com.mldeployapp.IntelImage.model.PathWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
@AllArgsConstructor
public class IntelImageService {

    private final IntelImagePredictor predictor;

    public void getPredictions(PathWrapper input) throws IOException {
        String predictedFileDir = input.getPredictedFileDir();
        File modelFileName = input.getModelFileName();

        predictor.predict(predictedFileDir, modelFileName);
    }

    public int trainModel() throws IOException {
        IntelImageClassifier.trainClassifier();
        return 0;
    }

}
